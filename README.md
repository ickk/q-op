q-op
====
This project currently exists just as a visualisation tool for the effect of a few main quantum gates on a single qbit. Eventually hopes to be a fun way to learn how quantum computers work.

Motivation:
- Learn about quantum computing.

Goals:
- Create a visualisation tool for understanding and playing with quantum computer programs.
- Eventually create some kind of novel experience, such as a short programming puzzle game (like zachtronics titles, but for quantum).

Todo:
- Further research into intuitive visualisations of entangled particles (see research.txt).
- implement quantum gates that operate on multiple qbits (CNOT in particular).
- See just how many qbits can be simulated on a regular computer (the math blows up quickly).
- implement a program/script reader for a 'quantum asm'.
- create puzzels to be solved.