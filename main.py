#!/usr/bin/env python3
from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.arrays import vbo
import numpy as np
from math import pi, cos, sin, tan, asin, atan, degrees, sqrt
import pygame
from pygame.locals import *
import cmath

def main():
	pygame.init()
	resolution = (800, 600)
	pygame.display.set_mode(resolution, DOUBLEBUF|OPENGL)

	aspect_ratio = resolution[0]/resolution[1]
	#gluPerspective(45, aspect_ratio, 0.1, 50.0)
	#gluPerspective(45, aspect_ratio, 0.1, 50.0)
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-4, 4, -3, 3, 0.0, 50.0);
	glEnable(GL_DEPTH_TEST); glDepthFunc(GL_LESS)
	glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

	# Move everything back by 5 units
	glTranslatef(0, 0, -16)

	# box = Box(1,1,1)
	q0 = Qubit()
	bloch_q0 = Bloch_sphere()
	q0.attach(bloch_q0)
	# q1 = Qubit()
	# bloch_q1 = Bloch_sphere()
	# q1.attach(bloch_q1)
	# q2 = Qubit()
	# bloch_q2 = Bloch_sphere()
	# q2.attach(bloch_q2)

	H_gate = Gate([[1/sqrt(2), 1/sqrt(2)], # Hadamard
	               [1/sqrt(2),-1/sqrt(2)]])
	X_gate = Gate([[0,  1], # NOT gate, or Pauli-X
	               [1,  0]])
	Y_gate = Gate([[0,-1j], # Pauli-Y
	               [1j, 0]])
	Z_gate = Gate([[1,  0], # Pauli-Z
	               [0, -1]])
	T_gate = Gate([[1,  0], # Pi/8 gate
	               [0,cmath.exp(1j*pi/4)]])
	S_gate = Gate([[1,  0], # Phase gate
	               [0,cmath.exp(1j*pi/2)]])

	# H_gate(q2)
	# X_gate(q2)
	# H_gate(q2)

	# bloch_q01 = Bloch_sphere()
	# bloch_q02 = Bloch_sphere()
	# bloch_q10 = Bloch_sphere()
	# bloch_q11 = Bloch_sphere()
	# bloch_q12 = Bloch_sphere()
	# bloch_q20 = Bloch_sphere()
	# bloch_q21 = Bloch_sphere()
	# bloch_q22 = Bloch_sphere()

	# Hadamard = Single_qubit_gate([[ 1/sqrt(2),  1/sqrt(2)],
	#                               [ 1/sqrt(2), -1/sqrt(2)]])
	# Pauli_X = Single_qubit_gate([[ 0,  1],
	#                              [ 1,  0]])
	# Pauli_Y = Single_qubit_gate([[ 0, -1j],
	#                              [ 1j, 0]])
	# Pauli_Z = Single_qubit_gate([[ 1,  0],
	#                              [ 0, -1]])
	# Phase_T = Single_qubit_gate([[ 1, 0], # Pi/8 gate
	#                              [ 0, cmath.exp(1j*pi/4)]])
	# Phase_S = Single_qubit_gate([[ 1, 0], # Phase gate
	#                              [ 0, cmath.exp(1j*pi/2)]])
	# H_theta, H_phi = Hadamard.apply(bloch_q00.theta, bloch_q00.phi)
	# bloch_q00.update(H_theta.real, H_phi.real)

	# H2_theta, H2_phi = Hadamard.apply(H_theta, H_phi)
	# bloch_q01.update(H2_theta.real, H2_phi.real)

	# X_theta, X_phi = Pauli_X.apply(bloch_q02.theta, bloch_q02.phi)
	# bloch_q02.update(X_theta.real, X_phi.real)

	# Y_theta, Y_phi = Pauli_Y.apply(bloch_q10.theta, bloch_q10.phi)
	# bloch_q10.update(Y_theta.real, Y_phi.real)

	# Z_theta, Z_phi = Pauli_Z.apply(bloch_q11.theta, bloch_q11.phi)
	# bloch_q11.update(Z_theta.real, Z_phi.real)

	# T_theta, T_phi = Phase_T.apply(bloch_q12.theta, bloch_q12.phi)
	# bloch_q12.update(T_theta.real, T_phi.real)

	# Z_theta, Z_phi = Pauli_Z.apply(bloch_q02.theta, bloch_q02.phi)
	# bloch_q20.update(Z_theta.real, Z_phi.real)

	# T_theta, T_phi = Phase_T.apply(bloch_q20.theta, bloch_q20.phi)
	# bloch_q21.update(T_theta.real, T_phi.real)

	# S_theta, S_phi = Phase_S.apply(bloch_q20.theta, bloch_q20.phi)
	# bloch_q22.update(S_theta.real, S_phi.real)

	# Initial state
	view_rotation = [15,-15,0]
	view_rotation_velocity = [0,0]

	# Main event loop
	glPushMatrix()
	while True:
		for event in pygame.event.get():
			if event.type == pygame.QUIT:
				pygame.quit()
				quit()
			elif event.type == pygame.KEYDOWN:
				if event.key == pygame.K_LEFT or event.key == pygame.K_n:
					view_rotation_velocity[1] += 1
				elif event.key == pygame.K_RIGHT or event.key == pygame.K_i:
					view_rotation_velocity[1] -= 1
				elif event.key == pygame.K_UP or event.key == pygame.K_u:
					view_rotation_velocity[0] += 1
				elif event.key == pygame.K_DOWN or event.key == pygame.K_e:
					view_rotation_velocity[0] -= 1
			elif event.type == pygame.KEYUP:
				if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
					print("quitting..")
					pygame.quit()
					quit()
				elif event.key == pygame.K_LEFT or event.key == pygame.K_n:
					view_rotation_velocity[1] -= 1
				elif event.key == pygame.K_RIGHT or event.key == pygame.K_i:
					view_rotation_velocity[1] += 1
				elif event.key == pygame.K_UP or event.key == pygame.K_u:
					view_rotation_velocity[0] -= 1
				elif event.key == pygame.K_DOWN or event.key == pygame.K_e:
					view_rotation_velocity[0] += 1
				# gates
				elif event.key == pygame.K_x:
					X_gate(q0)
				elif event.key == pygame.K_y:
					Y_gate(q0)
				elif event.key == pygame.K_z:
					Z_gate(q0)
				elif event.key == pygame.K_h:
					H_gate(q0)
				elif event.key == pygame.K_t:
					T_gate(q0)
				elif event.key == pygame.K_s:
					S_gate(q0)

		# Draw world
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)
		# Calculate rotation of view
		temp = view_rotation_velocity[0]+view_rotation[0]
		if temp > 90:
			view_rotation[0] = 90
		elif temp < -90:
			view_rotation[0] = -90
		else:
			view_rotation[0] = temp
		view_rotation[1] = (view_rotation[1]+view_rotation_velocity[1])%360
		# Update

		#bloch_q0.theta += pi/180
		#bloch_q0.phi += pi/180
		#bloch_q0.update(3*pi/4,3*pi/2)
		#print(bloch_q1.theta, bloch_q1.phi)
		#H_theta, H_phi = Hadamard.apply(bloch_q1.theta, bloch_q1.phi)
		#X_theta, X_phi = Pauli_X.apply(bloch_q2.theta, bloch_q2.phi)
		#bloch_q2.update(X_theta, X_phi)
		# Draw
		glPushMatrix()
		# glTranslatef(-2.5, 2, 0)
		bloch_q0.draw(view_rotation)
		# glTranslatef( 2.5, 0, 0)
		# bloch_q1.draw(view_rotation)
		# glTranslatef( 2.5, 0, 0)
		# bloch_q2.draw(view_rotation)
		#glTranslatef(-5, -2, 0)
		#bloch_q10.draw(view_rotation)
		#glTranslatef( 2.5, 0, 0)
		#bloch_q11.draw(view_rotation)
		#glTranslatef( 2.5, 0, 0)
		#bloch_q12.draw(view_rotation)
		#glTranslatef(-5, -2, 0)
		#bloch_q20.draw(view_rotation)
		#glTranslatef( 2.5, 0, 0)
		#bloch_q21.draw(view_rotation)
		#glTranslatef( 2.5, 0, 0)
		#bloch_q22.draw(view_rotation)
		glPopMatrix()

		# Draw models
		"""
		try:
			box.vbo.bind()
			glEnableClientState(GL_VERTEX_ARRAY)
			glVertexPointer(3, GL_FLOAT, 24, box.vbo)
			glDrawElements(GL_LINES, len(box.lines_indices), \
			               GL_UNSIGNED_INT, box.lines_indices)
		finally:
			box.vbo.unbind()
			glDisableClientState(GL_VERTEX_ARRAY)
		"""
		#box.draw_faces()

		# Finally
		pygame.display.flip()
		pygame.time.wait(17)

class Qubit():
	def __init__(self):
		"""Initialise a Qubit object"""
		self._state = [1, 0]
		self._theta = 0
		self._phi = 0
		self._bloch = None
	@property
	def theta(self):
		return self._theta
	@property
	def phi(self):
		return self._phi
	@property
	def state(self):
		return self._state
	@state.setter
	def state(self, value):
		self._state = value
		# math error fix
		if abs(self._state[0]) < 1e-05:
			self._state[0] = 0
		elif abs(self._state[0]-1) < 1e-05:
			self._state[0] = 1
		elif abs(self._state[0]-1j) < 1e-05:
			self._state[0] = 1j
		elif abs(self._state[0]+1) < 1e-05:
			self._state[0] = -1
		elif abs(self._state[0]+1j) < 1e-05:
			self._state[0] = -1j
		if abs(self._state[1]) < 1e-05:
			self._state[1] = 0
		elif abs(self._state[1]-1) < 1e-05:
			self._state[1] = 1
		elif abs(self._state[1]-1j) < 1e-05:
			self._state[1] = 1j
		elif abs(self._state[1]+1) < 1e-05:
			self._state[1] = -1
		elif abs(self._state[1]+1j) < 1e-05:
			self._state[1] = -1j
		# update theta and phi for drawing
		if abs(self._state[0]) == 1:  self._theta = 0
		else:                         self._theta = (2*cmath.acos(self._state[0])).real
		# bring theta back into range 0<=theta<=pi
		try:
			if self._theta == 0:
				print("Divide by zero error; leaving phi as last value.")
			else:
				intermediate = cmath.sin(self._theta/2) # this value may cause division by zero error or a math domain error
				self._phi = (-1j*cmath.log(self._state[1]/intermediate)).real
		except ValueError:
			print("state:",self._state)
			print("theta:",self._theta)
			print("intermediate:",intermediate)
			raise ValueError
		# if Bloch_sphere attached, update
		if self._bloch is not None:
			self._bloch.update(theta=self._theta, phi=self._phi)
	def attach(self, bloch_sphere):
		if isinstance(bloch_sphere, Bloch_sphere):
			self._bloch = bloch_sphere
		else:
			raise ValueError("Expected a Bloch_sphere object, but got "+repr(bloch_sphere))
	def detach(self):
		self._bloch = None

def Gate(matrix):
	"""Create a qubit gate that implements the transform given as matrix."""
	try: # define 1 qubit gate
		if len(matrix) == 2 and len(matrix[0]) == 2:
			def gate(qubit):
				new_state = [matrix[0][0]*qubit.state[0] + matrix[0][1]*qubit.state[1],
				             matrix[1][0]*qubit.state[0] + matrix[1][1]*qubit.state[1]]
				qubit.state = new_state
			return gate
		else:
			raise NotImplemented("Only supports the definition of single qubit gates.")
	except Exception as e:
		raise e

class Bloch_sphere():
	def update(self, theta, phi):
		self.theta = theta
		self.phi = phi
	def draw(self, rotation):
		try:
			self.vbo.bind()
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			glVertexPointer(3, GL_FLOAT, 28, self.vbo)
			glColorPointer(4, GL_FLOAT, 28, self.vbo+12)		
			# rotate view
			glPushMatrix()
			glRotatef(rotation[0],1,0,0) # x
			glRotatef(rotation[1],0,1,0) # y
			glRotatef(rotation[2],0,0,1) # z
			# draw direction vector
			glPushMatrix()
			glRotatef(degrees(self.phi),0,1,0)
			glRotatef(degrees(self.theta),1,0,0)
			glDrawElements(GL_QUADS, len(self.dirvec_tri_indices), \
			               GL_UNSIGNED_INT, self.dirvec_tri_indices)
			glPopMatrix()
			# draw circles
			for loop in self.line_loop_indices:
				glDrawElements(GL_LINE_LOOP, len(loop), \
				               GL_UNSIGNED_INT, loop)
			# draw axes
			glDrawElements(GL_LINES, len(self.lines_indices), \
			               GL_UNSIGNED_INT, self.lines_indices)
			glPopMatrix()
			# draw static circle
			glDrawElements(GL_TRIANGLE_FAN, len(self.tri_fan_indices), \
			               GL_UNSIGNED_INT, self.tri_fan_indices)
		finally:
			self.vbo.unbind()
			glDisableClientState(GL_VERTEX_ARRAY)
			glDisableClientState(GL_COLOR_ARRAY)
			
	def __init__(self):
		self.theta = 0
		self.phi = 0
		self.vbo = vbo.VBO(np.array([
			# direction vector
			# x             y              z  r    g    b    a
			[-0.05,         0,             0, 0.6, 0.2, 0.0, 1.0],
			[-0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0,            1,          0.05, 0.2, 0.0, 0.0, 1.0],
			[ 0,            0,          0.05, 0.2, 0.0, 0.0, 1.0],

			[ 0,            0,          0.05, 0.2, 0.0, 0.0, 1.0],
			[ 0,            1,          0.05, 0.2, 0.0, 0.0, 1.0],
			[ 0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0.05,         0,             0, 0.6, 0.2, 0.0, 1.0],

			[ 0.05,         0,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0,            1,         -0.05, 1.0, 0.8, 0.5, 1.0],
			[ 0,            0,         -0.05, 1.0, 0.8, 0.5, 1.0],

			[ 0,            0,         -0.05, 1.0, 0.8, 0.5, 1.0],
			[ 0,            1,         -0.05, 1.0, 0.8, 0.5, 1.0],
			[-0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[-0.05,         0,             0, 0.6, 0.2, 0.0, 1.0],

			[-0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0,            1,          0.05, 0.2, 0.0, 0.0, 1.0],
			[ 0.05,         1,             0, 0.6, 0.2, 0.0, 1.0],
			[ 0,            1,         -0.05, 1.0, 0.8, 0.5, 1.0],

			# axes
			[ 0,            0,             0, 1.0, 0.3, 0.0, 1.0], # +x
			[ 0,            0,           1.4, 1.0, 0.3, 0.0, 1.0],
			[ 0,            0,             0, 0.6, 1.0, 0.0, 1.0], # +y
			[ 1.4,          0,             0, 0.6, 1.0, 0.0, 1.0],
			[ 0,            0,             0, 0.0, 0.4, 1.0, 1.0], # +z
			[ 0,          1.4,             0, 0.0, 0.4, 1.0, 1.0],
			[ 0,            0,             0, 1.0, 1.0, 1.0, 0.4], # -x
			[ 0,            0,         -1.05, 1.0, 1.0, 1.0, 0.4],
			[ 0,            0,             0, 1.0, 1.0, 1.0, 0.4], # -y
			[-1.05,         0,             0, 1.0, 1.0, 1.0, 0.4],
			[ 0,            0,             0, 1.0, 1.0, 1.0, 0.4], # -z
			[ 0,        -1.05,             0, 1.0, 1.0, 1.0, 0.4],
			# x-y circle
			[cos( 0*pi/24), sin( 0*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #0
			[cos( 1*pi/24), sin( 1*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 2*pi/24), sin( 2*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 3*pi/24), sin( 3*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 4*pi/24), sin( 4*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 5*pi/24), sin( 5*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 6*pi/24), sin( 6*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #6
			[cos( 7*pi/24), sin( 7*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 8*pi/24), sin( 8*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos( 9*pi/24), sin( 9*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(10*pi/24), sin(10*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(11*pi/24), sin(11*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(12*pi/24), sin(12*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #12
			[cos(13*pi/24), sin(13*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(14*pi/24), sin(14*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(15*pi/24), sin(15*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(16*pi/24), sin(16*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(17*pi/24), sin(17*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(18*pi/24), sin(18*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #18
			[cos(19*pi/24), sin(19*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(20*pi/24), sin(20*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(21*pi/24), sin(21*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(22*pi/24), sin(22*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(23*pi/24), sin(23*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(24*pi/24), sin(24*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #24
			[cos(25*pi/24), sin(25*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(26*pi/24), sin(26*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(27*pi/24), sin(27*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(28*pi/24), sin(28*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(29*pi/24), sin(29*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(30*pi/24), sin(30*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #30
			[cos(31*pi/24), sin(31*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(32*pi/24), sin(32*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(33*pi/24), sin(33*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(34*pi/24), sin(34*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(35*pi/24), sin(35*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(36*pi/24), sin(36*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #36
			[cos(37*pi/24), sin(37*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(38*pi/24), sin(38*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(39*pi/24), sin(39*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(40*pi/24), sin(40*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(41*pi/24), sin(41*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(42*pi/24), sin(42*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #42
			[cos(43*pi/24), sin(43*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(44*pi/24), sin(44*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(45*pi/24), sin(45*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(46*pi/24), sin(46*pi/24), 0, 1.0, 1.0, 1.0, 0.4],
			[cos(47*pi/24), sin(47*pi/24), 0, 1.0, 1.0, 1.0, 0.4], #47
			# y-z circle
			[0, cos( 0*pi/24), sin( 0*pi/24), 1.0, 1.0, 1.0, 0.4], #48
			[0, cos( 1*pi/24), sin( 1*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 2*pi/24), sin( 2*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 3*pi/24), sin( 3*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 4*pi/24), sin( 4*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 5*pi/24), sin( 5*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 6*pi/24), sin( 6*pi/24), 1.0, 1.0, 1.0, 0.4], #54
			[0, cos( 7*pi/24), sin( 7*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 8*pi/24), sin( 8*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos( 9*pi/24), sin( 9*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(10*pi/24), sin(10*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(11*pi/24), sin(11*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(12*pi/24), sin(12*pi/24), 1.0, 1.0, 1.0, 0.4], #60
			[0, cos(13*pi/24), sin(13*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(14*pi/24), sin(14*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(15*pi/24), sin(15*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(16*pi/24), sin(16*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(17*pi/24), sin(17*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(18*pi/24), sin(18*pi/24), 1.0, 1.0, 1.0, 0.4], #66
			[0, cos(19*pi/24), sin(19*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(20*pi/24), sin(20*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(21*pi/24), sin(21*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(22*pi/24), sin(22*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(23*pi/24), sin(23*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(24*pi/24), sin(24*pi/24), 1.0, 1.0, 1.0, 0.4], #72
			[0, cos(25*pi/24), sin(25*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(26*pi/24), sin(26*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(27*pi/24), sin(27*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(28*pi/24), sin(28*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(29*pi/24), sin(29*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(30*pi/24), sin(30*pi/24), 1.0, 1.0, 1.0, 0.4], #78
			[0, cos(31*pi/24), sin(31*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(32*pi/24), sin(32*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(33*pi/24), sin(33*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(34*pi/24), sin(34*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(35*pi/24), sin(35*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(36*pi/24), sin(36*pi/24), 1.0, 1.0, 1.0, 0.4], #84
			[0, cos(37*pi/24), sin(37*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(38*pi/24), sin(38*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(39*pi/24), sin(39*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(40*pi/24), sin(40*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(41*pi/24), sin(41*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(42*pi/24), sin(42*pi/24), 1.0, 1.0, 1.0, 0.4], #90
			[0, cos(43*pi/24), sin(43*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(44*pi/24), sin(44*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(45*pi/24), sin(45*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(46*pi/24), sin(46*pi/24), 1.0, 1.0, 1.0, 0.4],
			[0, cos(47*pi/24), sin(47*pi/24), 1.0, 1.0, 1.0, 0.4], #95
			# x-z circle
			[cos( 0*pi/24), 0, sin( 0*pi/24), 1.0, 1.0, 1.0, 0.4], #96
			[cos( 1*pi/24), 0, sin( 1*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 2*pi/24), 0, sin( 2*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 3*pi/24), 0, sin( 3*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 4*pi/24), 0, sin( 4*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 5*pi/24), 0, sin( 5*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 6*pi/24), 0, sin( 6*pi/24), 1.0, 1.0, 1.0, 0.4], #102
			[cos( 7*pi/24), 0, sin( 7*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 8*pi/24), 0, sin( 8*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos( 9*pi/24), 0, sin( 9*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(10*pi/24), 0, sin(10*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(11*pi/24), 0, sin(11*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(12*pi/24), 0, sin(12*pi/24), 1.0, 1.0, 1.0, 0.4], #108
			[cos(13*pi/24), 0, sin(13*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(14*pi/24), 0, sin(14*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(15*pi/24), 0, sin(15*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(16*pi/24), 0, sin(16*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(17*pi/24), 0, sin(17*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(18*pi/24), 0, sin(18*pi/24), 1.0, 1.0, 1.0, 0.4], #114
			[cos(19*pi/24), 0, sin(19*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(20*pi/24), 0, sin(20*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(21*pi/24), 0, sin(21*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(22*pi/24), 0, sin(22*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(23*pi/24), 0, sin(23*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(24*pi/24), 0, sin(24*pi/24), 1.0, 1.0, 1.0, 0.4], #120
			[cos(25*pi/24), 0, sin(25*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(26*pi/24), 0, sin(26*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(27*pi/24), 0, sin(27*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(28*pi/24), 0, sin(28*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(29*pi/24), 0, sin(29*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(30*pi/24), 0, sin(30*pi/24), 1.0, 1.0, 1.0, 0.4], #126
			[cos(31*pi/24), 0, sin(31*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(32*pi/24), 0, sin(32*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(33*pi/24), 0, sin(33*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(34*pi/24), 0, sin(34*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(35*pi/24), 0, sin(35*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(36*pi/24), 0, sin(36*pi/24), 1.0, 1.0, 1.0, 0.4], #132
			[cos(37*pi/24), 0, sin(37*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(38*pi/24), 0, sin(38*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(39*pi/24), 0, sin(39*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(40*pi/24), 0, sin(40*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(41*pi/24), 0, sin(41*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(42*pi/24), 0, sin(42*pi/24), 1.0, 1.0, 1.0, 0.4], #138
			[cos(43*pi/24), 0, sin(43*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(44*pi/24), 0, sin(44*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(45*pi/24), 0, sin(45*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(46*pi/24), 0, sin(46*pi/24), 1.0, 1.0, 1.0, 0.4],
			[cos(47*pi/24), 0, sin(47*pi/24), 1.0, 1.0, 1.0, 0.4], #143
			# static circle
			[cos( 0*pi/24), sin( 0*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #144
			[cos( 1*pi/24), sin( 1*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 2*pi/24), sin( 2*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 3*pi/24), sin( 3*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 4*pi/24), sin( 4*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 5*pi/24), sin( 5*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 6*pi/24), sin( 6*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #150
			[cos( 7*pi/24), sin( 7*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 8*pi/24), sin( 8*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos( 9*pi/24), sin( 9*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(10*pi/24), sin(10*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(11*pi/24), sin(11*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(12*pi/24), sin(12*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #156
			[cos(13*pi/24), sin(13*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(14*pi/24), sin(14*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(15*pi/24), sin(15*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(16*pi/24), sin(16*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(17*pi/24), sin(17*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(18*pi/24), sin(18*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #162
			[cos(19*pi/24), sin(19*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(20*pi/24), sin(20*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(21*pi/24), sin(21*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(22*pi/24), sin(22*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(23*pi/24), sin(23*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(24*pi/24), sin(24*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #168
			[cos(25*pi/24), sin(25*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(26*pi/24), sin(26*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(27*pi/24), sin(27*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(28*pi/24), sin(28*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(29*pi/24), sin(29*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(30*pi/24), sin(30*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #174
			[cos(31*pi/24), sin(31*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(32*pi/24), sin(32*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(33*pi/24), sin(33*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(34*pi/24), sin(34*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(35*pi/24), sin(35*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(36*pi/24), sin(36*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #180
			[cos(37*pi/24), sin(37*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(38*pi/24), sin(38*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(39*pi/24), sin(39*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(40*pi/24), sin(40*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(41*pi/24), sin(41*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(42*pi/24), sin(42*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #186
			[cos(43*pi/24), sin(43*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(44*pi/24), sin(44*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(45*pi/24), sin(45*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(46*pi/24), sin(46*pi/24), 0, 0.5, 0.5, 0.5, 0.4],
			[cos(47*pi/24), sin(47*pi/24), 0, 0.5, 0.5, 0.5, 0.4], #191
			], dtype="f4"))
		self.dirvec_tri_indices = list(range(0,20))
		self.lines_indices = list(range(20,32))
		self.line_loop_indices = [ # x-y circle
		                          list(range(32,80)),
		                           # y-z circle
		                          list(range(80,128)),
		                           # x-z circle
		                          list(range(128,176))]
		self.tri_fan_indices = list(range(176,224))

class Box():
	def __init__(self, x=1, y=1, z=1):
		x = x/2
		y = y/2
		z = z/2
		self.vbo = vbo.VBO(np.array([
			[ x,  y,  z, 1,1,1,0.8],
			[-x,  y,  z, 0,1,1,0.8],
			[-x, -y,  z, 0,0,1,0.8],
			[ x, -y,  z, 1,0,1,0.8],
			[ x, -y, -z, 1,0,0,0.8],
			[-x, -y, -z, 0,0,0,0.8],
			[-x,  y, -z, 0,1,0,0.8],
			[ x,  y, -z, 1,1,0,0.8],
			], dtype="f4"))
		self.lines_indices = [0,1, 0,3, 0,7, 1,2, 1,6, 2,3, \
		                      2,5, 3,4, 4,5, 4,7, 5,6, 6,7]
		self.tri_strip_indices = [1,2,0,3,4,2,5,1,6,0,7,4,6,5]
	def draw_lines(self):
		try:
			self.vbo.bind()
			glEnableClientState(GL_VERTEX_ARRAY)
			glVertexPointer(3, GL_FLOAT, 24, self.vbo)
			glDrawElements(GL_LINES, len(self.lines_indices), GL_UNSIGNED_INT, self.lines_indices)
		finally:
			self.vbo.unbind()
			glDisableClientState(GL_VERTEX_ARRAY)
	def draw_faces(self):
		try:
			self.vbo.bind()
			glEnableClientState(GL_VERTEX_ARRAY)
			glEnableClientState(GL_COLOR_ARRAY)
			#glEnableClientState(GL_)
			glVertexPointer(3, GL_FLOAT, 28, self.vbo)
			glColorPointer(4, GL_FLOAT, 28, self.vbo+12)
			glDrawElements(GL_TRIANGLE_STRIP, len(self.tri_strip_indices), GL_UNSIGNED_INT, self.tri_strip_indices)
		finally:
			self.vbo.unbind()
			glDisableClientState(GL_VERTEX_ARRAY)
			glDisableClientState(GL_COLOR_ARRAY)

class Single_qubit_gate():
	def __init__(self, matrix):
		self.matrix = matrix
	def apply(self, theta, phi):
		# ( |0⟩,
		#   |1⟩ )
		state = (cmath.cos(theta/2),\
		         cmath.exp(1j*phi)*cmath.sin(theta/2))
		new_state = (self.matrix[0][0]*state[0] + self.matrix[0][1]*state[1],\
		             self.matrix[1][0]*state[0] + self.matrix[1][1]*state[1])
		new_theta = 2*cmath.acos(new_state[0])
		if new_theta == 0:
			new_phi = phi
		else:
			new_phi = -1j*cmath.log(new_state[1]/cmath.sin(new_theta/2))
		return new_theta, new_phi

if __name__ == "__main__":
	main()
